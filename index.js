export function flatten(array) {
  if (!Array.isArray(array)) {
    throw new Error("Invalid paramter.");
  }

  const flattened = [];
  const r = function (item) {
    if (Array.isArray(item)) {
      item.forEach((e) => r(e));
    } else {
      flattened.push(item);
    }
  };
  array.forEach((item) => r(item));
  return flattened;
}
