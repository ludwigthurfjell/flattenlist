import { describe, expect, it } from "@jest/globals";
import { flatten } from "./index";

describe("flatten", () => {
  it("[] returns []", () => {
    const actual = flatten([]);
    const expected = [];
    expect(actual).toStrictEqual(expected);
  });
  it("[1,2] returns [1,2]", () => {
    const actual = flatten([1, 2]);
    const expected = [1, 2];
    expect(actual).toStrictEqual(expected);
  });

  it("[1,[2,3]] returns [1,2,3]", () => {
    const actual = flatten([1, [2, 3]]);
    const expected = [1, 2, 3];
    expect(actual).toStrictEqual(expected);
  });
  
  it("[1,[2,[3]]] returns [1,2,3]", () => {
    const actual = flatten([1,[2,[3]]]);
    const expected = [1, 2, 3];
    expect(actual).toStrictEqual(expected);
  });

  it("[[[1,2,3]]] returns [1,2,3]", () => {
    const actual = flatten([[[1, 2, 3]]]);
    const expected = [1, 2, 3];
    expect(actual).toStrictEqual(expected);
  });
  
  it("[[[]]] returns []", () => {
    const actual = flatten([[[]]]);
    const expected = [];
    expect(actual).toStrictEqual(expected);
  });
  
  it("[[],[]] returns []", () => {
    const actual = flatten([[],[]]);
    const expected = [];
    expect(actual).toStrictEqual(expected);
  });
});
